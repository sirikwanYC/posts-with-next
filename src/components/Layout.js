import '../style/layout.less'

export default (props) => (
    <div className="layout">
        <div className="card">
            {props.children}
        </div>
    </div>
)