import React, { Component } from 'react'
import { Icon, Button, Input } from 'antd'
import Layout from '../src/components/Layout'
import '../src/style/index.less'

class index extends Component {
    state = {
        posts: [
            {
                threadOwner: 'สมาชิก 1',
                namePost: 'กระทู้ 1',
                contentPost: 'สวัสดีตอนเช้าค่ะ อากาศสดใสดีนะะ',
                messagePost: [
                    {
                        nameAnswerPost: 'อิอิ',
                        answerPost: 'สวัสดีจ้าาา'
                    },
                    {
                        nameAnswerPost: 'อิอิ2',
                        answerPost: 'สวัสดีค่ะ'
                    }
                ]
            },
            {
                threadOwner: 'สมาชิก 2',
                namePost: 'กระทู้ 2',
                contentPost: 'มาคุยกันนนน',
                messagePost: [
                    {
                        nameAnswerPost: 'ฟ้าสดใส',
                        answerPost: 'คุยไรเหรอ ?'
                    },
                    {
                        nameAnswerPost: 'ไม่บอกหรอก',
                        answerPost: 'ไม่คุยได้ป่ะ ?'
                    }
                ]
            },
            {
                threadOwner: 'LilyBlossom',
                namePost: 'เหตุการณ์เบลอๆ เรื่องไหนของคุณที่แบบว่าไหวป่ะ',
                contentPost: 'ของเราเวลาทำงานมึนๆ อ่านหนังสือสอบ นอนดึกตื่นเช้า, -ใส่คอนแทคเลนส์ข้างเดียวกัน 2 อัน ก็ว่าใส่ไปแล้วทำไมมันไม่ชัดหว่า ขยี้ตาดูนึกว่าคอนแทคเลนส์ไม่ตรง อ้าว ใส่ข้างนี้ไปแล้วนี่หว่า, -เอาครีมอาบน้ำไปสระผม ถึงว่าทำไมกลิ่นแปลกๆ',
                messagePost: [
                    {
                        nameAnswerPost: 'Aqua Plankton',
                        answerPost: 'เอาเจลล้างหน้าไปสระผม เอายาสระผมมาถูตัว, ถือปากกาอยู่ในมือ หาตั้งนานใครเห็นปากกาบ้างงงงง??? ปากกาหายไปไหน, ทาครีมกันแดดก่อนนอน - -"'
                    },
                    {
                        nameAnswerPost: 'น้องถั่วน้อย',
                        answerPost: 'เราเคยหาแว่นไม่เจอ ตีโพยตีพาย ว่าแว่นหาย หาเท่าไหร่ก็ไม่เจอ ที่ไหนได้ เฮอะ!! ก็ใส่อยู่ที่ตาเนี่ยแหละ ต้องมีใครแกล้งเราแน่ ๆ'
                    },
                    {
                        nameAnswerPost: 'Lolipop_Nun',
                        answerPost: 'ออกจากที่จอดรถแล้วรับโทรศัพท์พอดี จังหวะที่ยื่นบัตรคืนกลายเป็นว่าเบลอ ยื่นโทรศัพท์ให้เม่าโศกเม่าโศกแทน ไอ่เราก็ขับรถออกมาเลยนึกขึ้นได้ จอดรถวิ่งไปเอามาคืน คิดแล้วยังฮาไม่หาย'
                    }
                ]
            },
        ],
        inputMessage: '',
        inputName: '',
        inputNamePost: '',
        addPost: false,
    }

    onClickNamePost = (index) => {
        this.setState({
            inputMessage: '',
            inputName: '',
            inputNamePost: '',
        })
        return document.getElementById(`post${index}`).scrollIntoView();
    }

    splitText = (text) => {
        const arrText = text.split(", ")
        return arrText
    }

    onChangeInputName = (e) => {
        this.setState({
            inputName: e.target.value
        })
    }

    onChangeInputMessage = (e) => {
        this.setState({
            inputMessage: e.target.value
        })
    }
    onChangeInputNamePost = (e) => {
        this.setState({
            inputNamePost: e.target.value
        })
    }

    onClickButtonSendMessage = (index) => {
        let { posts, inputMessage, inputName } = this.state
        const arrNew = []

        posts.forEach((value, i) => {

            if (index === i) {
                arrNew.push(
                    {
                        threadOwner: value.threadOwner,
                        namePost: value.namePost,
                        contentPost: value.contentPost,
                        messagePost: value.messagePost.concat({ nameAnswerPost: inputName, answerPost: inputMessage })
                    }
                )

            } else {
                arrNew.push(value)
            }

        })

        this.setState({
            posts: arrNew,
            inputName: '',
            inputMessage: ''
        })
    }

    addPost = () => {
        this.setState({
            addPost: true,
        })
        return document.getElementById('addPost').scrollIntoView();
    }

    onClickButtonAddPost = () => {
        const { posts, inputName, inputMessage, inputNamePost } = this.state
        this.setState({
            addPost: !addPost
        })
        this.setState({
            posts: posts.concat({
                threadOwner: inputName,
                namePost: inputNamePost,
                contentPost: inputMessage,
                messagePost: []
            })
        })
        return document.getElementById('posthome').scrollIntoView();
    }

    render() {
        return (
            <div className="posts">
                <div className="home" id="posthome">
                    <Layout>
                        <div className="head">
                            <div className="name-post">
                                <h1> กระทู้ทั้งหมด </h1>
                            </div>
                            <div className="back-home">
                                <Button type="primary" onClick={this.addPost}>  เพิ่มกระทู้ </Button>
                            </div>
                        </div>
                        <hr />
                        <div className="content">
                            {
                                this.state.posts.map((value, index) => {
                                    return (
                                        <div key={+index} className="all-post">
                                            <div className="icon-message">
                                                <Icon type="message" theme="twoTone" />
                                            </div>
                                            <div className="post-name">
                                                <Button type="primary" onClick={() => this.onClickNamePost(index)}> {value.namePost} </Button>
                                            </div>
                                        </div>
                                    )
                                })
                            }

                        </div>
                    </Layout>
                </div>
                <div className="add-post" id="addPost">
                    {
                        this.state.addPost ?
                            <div>
                                <Layout>
                                    <div className="head">
                                        <div className="name-post">
                                            <h1> เพิ่มกระทู้ </h1>
                                        </div>
                                        <div className="back-home">
                                            <Button type="primary" onClick={() => this.onClickNamePost('home')}> กลับหน้าหลัก </Button>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="message">
                                        <div className="input-name">
                                            <div className="text">
                                                <p>ชื่อ</p>
                                            </div>
                                            <div className="input-name">
                                                <Input value={this.state.inputName} onChange={this.onChangeInputName} />
                                            </div>
                                        </div>
                                        <div className="input-name-post">
                                            <div className="text">
                                                <p>ชื่อกระทู้</p>
                                            </div>
                                            <div className="input-name">
                                                <Input value={this.state.inputNamePost} onChange={this.onChangeInputNamePost} />
                                            </div>
                                        </div>
                                        <div className="input-answer">
                                            <div className="text">
                                                <p>ข้อความ</p>
                                            </div>
                                            <div className="input-message">
                                                <textarea value={this.state.inputMessage} onChange={this.onChangeInputMessage} />
                                            </div>
                                        </div>
                                        <div className="send-message-button">
                                            <Button onClick={this.onClickButtonAddPost}> ตั้งกระทู้ </Button>
                                        </div>
                                    </div>

                                </Layout>
                            </div>
                            :
                            ''
                    }
                </div>
                <div className="post">
                    {
                        this.state.posts.map((value, index) => {
                            return (
                                <div className="content-post" key={+index} id={`post${index}`}>
                                    <Layout>
                                        <div className="head">
                                            <div className="name-post">
                                                <div className="icon-message">
                                                    <Icon type="message" theme="twoTone" />
                                                </div>
                                                <div className="name">
                                                    <h1> {value.namePost} </h1>
                                                </div>
                                            </div>
                                            <div className="back-home">
                                                <Button type="primary" onClick={() => this.onClickNamePost('home')}> กลับหน้าหลัก </Button>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="content">
                                            <div className="box-content">
                                                {value.contentPost && this.splitText(value.contentPost).map((v, i) => {
                                                    return (
                                                        <p key={+i}> {v} </p>
                                                    )
                                                })}
                                                <div className="thread-owner">
                                                    <p>โดย: {value.threadOwner} </p>
                                                </div>
                                            </div>
                                            <div className="opinion">
                                                <hr />
                                                <div className="opinion-text">
                                                    <p> ความคิดเห็น </p>
                                                </div>
                                            </div>
                                            <div className="message-post">
                                                {console.log(this.state.inputName)}
                                                {value.messagePost &&
                                                    value.messagePost.map((v, i) => {
                                                        return (
                                                            <div key={+i} className="message">
                                                                <div className="opinion-on">
                                                                    <p> ความคิดเห็นที่ {i + 1} </p>
                                                                </div>
                                                                <div className="text-answer">
                                                                    <div>
                                                                        {this.splitText(v.answerPost).map((ans, j) => {
                                                                            return (
                                                                                <p key={+j}> {ans} </p>
                                                                            )
                                                                        })}
                                                                    </div>
                                                                </div>
                                                                <div className="name-answer">
                                                                    <p>คุณ: {v.nameAnswerPost}</p>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                }
                                                <div className="opinion">
                                                    <hr />
                                                    <div className="opinion-text">
                                                        <p> แสดงความคิดเห็น </p>
                                                    </div>
                                                </div>
                                                <div className="message">
                                                    <div className="input-name">
                                                        <div className="text">
                                                            <p>ชื่อ</p>
                                                        </div>
                                                        <div className="input-name">
                                                            <Input value={this.state.inputName} onChange={this.onChangeInputName} />
                                                        </div>
                                                    </div>
                                                    <div className="input-answer">
                                                        <div className="text">
                                                            <p>ข้อความ</p>
                                                        </div>
                                                        <div className="input-message">
                                                            <textarea value={this.state.inputMessage} onChange={this.onChangeInputMessage} />
                                                        </div>
                                                    </div>
                                                    <div className="send-message-button">
                                                        <Button onClick={() => this.onClickButtonSendMessage(index)}> ส่งข้อความ </Button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Layout>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default index